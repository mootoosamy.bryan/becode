<?php

namespace Classes;

use Exception;

class Vehicle
{
    /**
     * @const array
     */
    protected const WHEELS_BY_TYPE = [
        'car' => 4
    ];

    /**
     * @var string
     */
    protected string $type;

    /**
     * Vehicle constructor.
     * @param string $type
     */
    public function __construct(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function countWheels(): int
    {
        if (!array_key_exists($this->type, self::WHEELS_BY_TYPE)) {
            throw new Exception('Vehicle type not supported');
        }

        return self::WHEELS_BY_TYPE[$this->type];
    }
}
