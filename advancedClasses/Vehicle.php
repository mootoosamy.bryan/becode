<?php

namespace AdvancedClasses;

use AdvancedClasses\Vehicles\Bike;
use AdvancedClasses\Vehicles\Car;
use AdvancedClasses\Vehicles\Tricycle;

class Vehicle
{
    /**
     * @var bool
     */
    protected bool $have_motor = true;

    /**
     * @const array
     */
    protected const SUPPORTED_CHILDREN = [
        Bike::class,
        Car::class,
        Tricycle::class
    ];

    /**
     * @return bool
     */
    public function haveMotor(): bool
    {
        return $this->have_motor;
    }

    /**
     * @param string $class
     * @return int
     * @throws \Exception
     */
    public function getWheelsByType(string $class): int
    {
        if (!in_array($class, self::SUPPORTED_CHILDREN, true)) {
            throw new \Exception('Type not supported');
        }

        return (new $class())->getWheelsCount();
    }
}
