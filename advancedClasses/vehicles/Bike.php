<?php

namespace AdvancedClasses\Vehicles;

use AdvancedClasses\Vehicle;

class Bike extends Vehicle
{
    /**
     * @var int
     */
    protected int $wheels_count = 2;

    /**
     * @return int
     */
    public function getWheelsCount(): int
    {
        return $this->wheels_count;
    }
}
