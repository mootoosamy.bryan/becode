<?php

namespace AdvancedClasses\Vehicles;

use AdvancedClasses\Vehicle;

class Car extends Vehicle
{
    /**
     * @var int
     */
    protected int $wheels_count = 4;

    /**
     * @return int
     */
    public function getWheelsCount(): int
    {
        return $this->wheels_count;
    }
}
