<?php

namespace AdvancedClasses\Vehicles;

use AdvancedClasses\Vehicle;

class Tricycle extends Vehicle
{
    /**
     * @var int
     */
    protected int $wheels_count = 3;

    /**
     * @return int
     */
    public function getWheelsCount(): int
    {
        return $this->wheels_count;
    }
}
